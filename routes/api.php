<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login')->name('login');
Route::get('/user', 'AuthController@user');
Route::post('/logout', 'AuthController@logout')->name('logout');

Route::post('/change_password', 'AuthController@changePassword')->name('change-password');
