<?php

return [
    'title-time-do-timesheet' => 'It\'s time to doing timesheet.',
    'please-do-timesheet' => 'Please do your timesheet.',
    'message-do-timesheet' => 'Make a timesheet for today before going home.',
    'btn-click-now' => 'Click to do timesheet now!',
    'title-time-complete-timesheet' => 'It\'s time to must have done timesheet.',
];
