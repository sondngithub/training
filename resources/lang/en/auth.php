<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email' => 'Email',
    'password' => 'Password',
    'login-success' => 'Logged in successfully.',
    'logout-success' => 'Logout successfully',
    'logout-error' => 'Logout error',
    'current-password' => 'Current Password',
    'new-password' => 'New Password',
    'password-confirmation' => 'Confirm Password',
    'change-password-success' => 'Changed password successfully!',
    'change-password-error' => 'Can not change your password!',
    'password-not-match' => 'Password is incorrect!'

];
