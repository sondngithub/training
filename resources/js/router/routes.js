import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";
import Login from "../pages/Login";
import ChangePassword from "../pages/ChangePassword";

// Admin pages
import Home from "../pages/Admin/Home";
import UserDetail from "../pages/UserDetail";

//Employee pages
import Dashboard from "../pages/Dashboard";

//Leader pages
import TimesheetsManagementByLeader from "../pages/Leader/Timesheets/Index";

const routes = [
    {
        path: "/login",
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: "/admin",
        component: DashboardLayout,
        redirect: {
            name: "admin-home"
        },
        meta: {
            auth: {roles: 'Admin', redirect: {name: 'login'}, forbiddenRedirect: '/403'}
        },
        children: [
            {
                path: "home",
                name: "admin-home",
                component: Home
            }
        ]
    },
    {
        path: "/",
        component: DashboardLayout,
        redirect: {
            name: "dashboard"
        },
        meta: {
            auth: {roles: 'Employee', redirect: {name: 'login'}, forbiddenRedirect: '/403'}
        },
        children: [
            {
                path: "dashboard",
                name: "dashboard",
                component: Dashboard
            },
            {
                path: "change_password",
                name: "change-password",
                component: ChangePassword,
            },
            {
                path: "users/:id",
                name: "user-detail",
                component: UserDetail,
            }
        ]
    },
    {
        path: "/leader/timesheets",
        component: DashboardLayout,
        redirect: {
            name: "dashboard"
        },
        meta: {
            auth: {roles: 'Leader', redirect: {name: 'login'}, forbiddenRedirect: '/403'}
        },
        children: [
            {
                path: "/",
                name: "leader.timesheets.index",
                component: TimesheetsManagementByLeader
            }
        ]
    },
    {
        path: "*",
        name: 'not-found',
        component: NotFound}
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
