<?php

namespace App\Http\Resources;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class UserResource extends JsonResource
{
    protected $tokenResult;
    protected $token;

    public function __construct($resource, $tokenResult = null, $token = null)
    {
        parent::__construct($resource);

        $this->tokenResult = $tokenResult;
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request = null)
    {
        return array_merge([
            'username' => $this->username,
            'email' => $this->email,
            'avatar' => $this->avatar ?? User::AVATAR_DEFAULT,
            'description' => $this->description ?? '',
            'roles' => RoleResource::collection($this->whenLoaded('roles'))->map(function ($role) {
                return $role->name;
            }),
        ], $this->mergeWhen(URL::current() === route('login') || URL::current() === route('change-password'), [
            'token_type' => 'Bearer',
            'access_token' => $this->tokenResult->accessToken ?? '',
            'expires_at' => Carbon::parse($this->token->expires_at ?? now())->toDateTimeString(),
        ])->data ?? []);
    }
}
