<?php

namespace App\Http\Resources;

use App\Models\Timesheet;
use Illuminate\Http\Resources\Json\JsonResource;

class TimesheetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'date' => $this->date,
            'status' => Timesheet::statuses($this->status),
            'trouble' => $this->trouble,
            'plan' => $this->plan,
            'user' => new UserResource($this->whenLoaded('user')),
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];

        if (!empty($this->username)) {
            $data['username'] = $this->username;
        }

        if (!empty($this->email)) {
            $data['email'] = $this->email;
        }

        return $data;
    }
}
