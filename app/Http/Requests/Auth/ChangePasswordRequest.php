<?php


namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class ChangePasswordRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return me() ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'                      => 'required|max:255|password:api',
            'new_password'                  => 'required|max:255|min:8|different:password|confirmed',
            'new_password_confirmation'     => 'required|min:8',
        ];
    }

    public function attributes()
    {
        return [
            'password'                      => __('auth.current-password'),
            'new_password'                  => __('auth.new-password'),
            'new_password_confirmation'     => __('auth.password-confirmation')
        ];
    }

}
