<?php

namespace App\Http\Requests\Timesheets;

use App\Http\Requests\BaseRequest;
use App\Models\Timesheet;

class DatatableRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'per_page' => 'nullable|numeric|gt:0|max:' . Timesheet::MAX_PER_PAGE,
            'search' => 'nullable|max:255',
            'sortColumn' => 'max:255',
            'direction' => 'nullable|in:asc,desc',
        ];
    }
}
