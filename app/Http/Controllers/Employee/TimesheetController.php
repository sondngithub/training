<?php

namespace App\Http\Controllers\Employee;

use App\Exceptions\ApiErrorException;
use App\Http\Controllers\BaseController;
use App\Models\Timesheet;
use App\Services\Interfaces\TimesheetServiceInterface;
use Illuminate\Http\Response;

class TimesheetController extends BaseController
{
    protected $timesheetService;

    public function __construct(TimesheetServiceInterface $timesheetService)
    {
        parent::__construct();

        $this->timesheetService = $timesheetService;
    }

    public function dashboard()
    {
        if (me()->cant('viewDashboard', Timesheet::class)) {
            throw new ApiErrorException(__('http_codes.403'), Response::HTTP_FORBIDDEN);
        }

        $data = $this->timesheetService->getStatisticInformation(me());

        return $this->responseSuccess(__('http_codes.200'), $data);
    }
}
