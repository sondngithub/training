<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiErrorException;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserResource;
use App\Services\Interfaces\AuthServiceInterface;

class AuthController extends BaseController
{
    protected $authService;

    public function __construct(AuthServiceInterface $authService)
    {
        parent::__construct();

        $this->middleware(['auth:api'], ['except' => 'login']);
        $this->authService = $authService;
    }

    public function login(LoginRequest $request)
    {
        $response = $this->authService->login($request);

        return $this->responseSuccess(__('auth.login-success'), $response);
    }

    public function user()
    {
        $response = new UserResource(me());

        return $this->responseSuccess(__('http_codes.200'), $response);
    }

    public function logout()
    {
        if (!$this->authService->logout()) {
            throw new ApiErrorException(__('auth.logout-error'));
        }

        return $this->responseSuccess(__('auth.logout-success'));
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $response = $this->authService->changePassword($request);
        if (!$response) {
            throw new ApiErrorException(__('auth.change-password-error'));
        }

        return $this->responseSuccess(__('auth.change-password-success'), $response);
    }
}
