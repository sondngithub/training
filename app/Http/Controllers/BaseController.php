<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use App\Traits\ApiResponsive;

class BaseController extends Controller
{
    use ApiResponsive;

    public function __construct()
    {
    }
}
