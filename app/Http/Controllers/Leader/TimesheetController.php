<?php

namespace App\Http\Controllers\Leader;

use App\Exceptions\ApiErrorException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Timesheets\DatatableRequest;
use App\Models\Timesheet;
use App\Services\Interfaces\TimesheetServiceInterface;
use Illuminate\Http\Response;

class TimesheetController extends BaseController
{
    protected $timesheetService;

    public function __construct(TimesheetServiceInterface $timesheetService)
    {
        parent::__construct();

        $this->timesheetService = $timesheetService;
    }

    public function index(DatatableRequest $request)
    {
        if (me()->cant('viewEmployeeTimesheets', Timesheet::class)) {
            throw new ApiErrorException(__('http_codes.403'), Response::HTTP_FORBIDDEN);
        }

        $timesheets = $this->timesheetService->getTimesheetsOfEmployeesByLeader(me(), $request);

        return $this->responseSuccessWithPagination($timesheets, __('http_codes.200'));
    }
}
