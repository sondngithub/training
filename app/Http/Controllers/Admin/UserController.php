<?php


namespace App\Http\Controllers\Admin;

use App\Exceptions\ApiErrorException;
use App\Models\User;
use App\Services\Interfaces\UserServiceInterface;
use App\Http\Controllers\BaseController;

class UserController extends BaseController
{
    protected $userService;

    public function __construct(UserServiceInterface $userService)
    {
        parent::__construct();

        $this->userService = $userService;
    }

    public function show(User $user)
    {
        if (!$this->userService->getUser($user)) {
            throw new ApiErrorException(__('user.not-found'));
        }

        return $this->userService->getUser($user);
    }
}
