<?php


namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponsive
{
    public function responseSuccessWithPagination($pagination,
                                                  string $message = "",
                                                  array $headers = [],
                                                  int $status = Response::HTTP_OK)
    {
        $data = $pagination->items();
        $meta = [
            'total' => $pagination->total(),
            'last_page' => $pagination->lastPage(),
            'per_page' => $pagination->perPage(),
            'current_page' => $pagination->currentPage(),
            'next_link' => $pagination->nextPageUrl(),
            'previous_link' => $pagination->previousPageUrl(),
        ];

        return $this->responseSuccess($message, $data, $meta, $headers, $status);
    }

    public function responseSuccess(string $message = "",
                                    $data = [],
                                    array $meta = [],
                                    array $headers = [],
                                    int $status = Response::HTTP_OK)
    {
        return $this->response('Success', $message, $data, $meta, $headers, $status);
    }

    public function responseError(string $message,
                                  array $meta = [],
                                  array $headers = [],
                                  int $status = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        return $this->response('Error', $message, [], $meta, $headers, $status);
    }

    protected function response(string $type = "",
                             string $message = "",
                             $data = [],
                             array $meta = [],
                             array $headers = [],
                             int $status = Response::HTTP_BAD_REQUEST)
    {
        return response()->json([
            'type' => $type,
            'message' => $message,
            'data' => $data,
            'meta' => $meta,
        ],  $status, $headers);
    }
}
