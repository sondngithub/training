<?php

namespace App\Services;

use App\Exceptions\ApiErrorException;
use App\Http\Resources\TimesheetResource;
use App\Models\Timesheet;
use App\Models\User;
use App\Services\Interfaces\TimesheetServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TimesheetService extends BaseService implements TimesheetServiceInterface
{
    /**
     * Get statistic information of a employee.
     * This information includes total timesheets count, late timesheets count, total tasks count and total actual hours
     * of each month in current year.
     *
     * @param User|null $employee
     * @param string $date
     * @return array[]
     */
    public function getStatisticInformation(User $employee = null)
    {
        if (!$employee) {
            throw new ApiErrorException(__('http_errors.403'));
        }

        return [
            'total' => $this->getTotalTimesheetsInCurrentYear($employee),
            'late' => $this->getTotalLateTimesheetsInCurrentYear($employee),
            'tasks_count' => $this->getTotalTasksInCurrentYear($employee),
            'actual_hours' => $this->getTotalActualHoursInCurrentYear($employee),
        ];
    }

    /**
     * Get total timesheet count of each month in current year.
     *
     * @param User $employee
     * @return \Illuminate\Database\Eloquent\Collection|Collection
     */
    public function getTotalTimesheetsInCurrentYear(User $employee)
    {
        return $employee->timesheets()
            ->whereYear('date', now()->format('Y'))
            ->select([
                DB::raw('MONTH(date) month')])
            ->get()
            ->groupBy('month')
            ->map(function ($item) {
                return $item->count();
            })
            ->sortKeys();
    }

    /**
     * Get total late timesheet count of each month in current year.
     *
     * @param User $employee
     * @return mixed
     */
    public function getTotalLateTimesheetsInCurrentYear(User $employee)
    {
        return $employee->timesheets()
            ->late()
            ->whereYear('date', now()->format('Y'))
            ->select([
                DB::raw('MONTH(date) month')])
            ->get()
            ->groupBy('month')
            ->map(function ($item) {
                return $item->count();
            })
            ->sortKeys();
    }

    /**
     * Get total tasks count of each month in current year.
     *
     * @param User $employee
     * @return \Illuminate\Database\Eloquent\Collection|Collection
     */
    public function getTotalTasksInCurrentYear(User $employee)
    {
        return $employee->timesheets()
            ->whereYear('date', now()->format('Y'))
            ->select([
                DB::raw('MONTH(date) month')
            ])
            ->withCount('tasks')
            ->get()
            ->groupBy('month')
            ->map(function ($item) {
                return $item->sum('tasks_count');
            })
            ->sortKeys();
    }

    /**
     * Get total actual hours of each month in current year.
     *
     * @param User $employee
     * @return \Illuminate\Database\Eloquent\Collection|Collection
     */
    public function getTotalActualHoursInCurrentYear(User $employee)
    {
        return $employee->timesheets()
            ->whereYear('date', now()->format('Y'))
            ->select([
                '*',
                DB::raw('MONTH(date) month')
            ])
            ->with('tasks')
            ->get()
            ->groupBy('month')
            ->map(function ($items) {
                $totalActualHours = 0;
                foreach ($items as $timesheet) {
                    $totalActualHours += $timesheet->tasks->sum('actual_hours');
                }

                return $totalActualHours;
            })
            ->sortKeys();
    }

    /**
     * Get timesheets of employees by leader, who managing these employees.
     * Except for approved timesheets.
     *
     * @param User $leader
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function getTimesheetsOfEmployeesByLeader(User $leader, Request $request)
    {
        $selectColumns = [
            'timesheets.*',
            'users.id',
            'users.username',
            'users.email',
        ];

        $searchColumns = [
            'timesheets.date',
            'timesheets.updated_at',
            'users.username',
            'users.email',
        ];

        $employeeIds = $leader->employees->pluck('id');

        $perPage = $request->get('per_page', Timesheet::DEFAULT_PER_PAGE);

        $query = Timesheet::select($selectColumns)
            ->join('users', 'users.id', '=', 'timesheets.user_id')
            ->whereIn('user_id', $employeeIds)
            ->where('status', '!=', Timesheet::STATUS_APPROVED);

        if ($search = $request->get('search')) {
            $query->where(function ($query) use ($searchColumns, $search) {
                foreach ($searchColumns as $column) {
                    $query->orWhere($column, 'LIKE', "%$search%");
                }
            });
        }

        if (($sortColumn = $request->get('sortColumn')) && ($direction = $request->get('direction'))) {
            $query->orderBy($sortColumn, $direction);
        } else {
            $query->orderBy('status')
                ->orderByDesc('date');
        }

        return TimesheetResource::collection($query
            ->paginate($perPage));
    }
}
