<?php


namespace App\Services;


use App\Exceptions\ApiErrorException;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserResource;
use App\Services\Interfaces\AuthServiceInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Token;
use App\Http\Requests\Auth\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;

class AuthService extends BaseService implements AuthServiceInterface
{
    public function login(LoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);
        $isRememberMe = $request->input('remember_me', false);

        if (! Auth::guard('web')->attempt($credentials)) {
            throw new ApiErrorException(__('auth.failed'));
        }

        $user = Auth::guard('web')->user();
        $createToken = $this->createToken($user, $request);

        return new UserResource($user, $createToken['tokenResult'], $createToken['token']);
    }

    protected function changeExpireTime(Token $token, int $periodDays = 1)
    {
        return $token->update([
            'expires_at' => Carbon::now()->addDays($periodDays)->toDateTimeString(),
        ]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = me();
        $user->password = Hash::make($request->new_password);
        $user->token()->revoke();
        $createToken = $this->createToken($user, $request);
        $user->save();

        return new UserResource($user, $createToken['tokenResult'], $createToken['token']);
    }

    public function createToken($user, $request){
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $isRememberMe = $request->input('remember_me', false);

        if ($isRememberMe) {
            if (! $this->changeExpireTime($token, 365)) {
                throw new ApiErrorException(__('http_codes.500'));
            }
        }

        return ['tokenResult' => $tokenResult,
                'token'       => $token,
            ];
    }

    public function logout()
    {
        return me()->token()->revoke();
    }
}
