<?php


namespace App\Services;

use App\Exceptions\ApiErrorException;
use App\Models\User;
use App\Services\Interfaces\UserServiceInterface;

class UserService extends BaseService implements UserServiceInterface
{
    public function getUser(User $user)
    {
        return $user->load('roles', 'leader');
    }
}
