<?php


namespace App\Services\Interfaces;
use App\Models\User;

interface UserServiceInterface
{
    public function getUser(User $user);
}
