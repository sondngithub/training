<?php

namespace App\Services\Interfaces;

use App\Models\User;
use Illuminate\Http\Request;

interface TimesheetServiceInterface
{
    public function getStatisticInformation(User $employee = null);
    public function getTotalTimesheetsInCurrentYear(User $employee);
    public function getTotalLateTimesheetsInCurrentYear(User $employee);
    public function getTotalTasksInCurrentYear(User $employee);
    public function getTotalActualHoursInCurrentYear(User $employee);
    public function getTimesheetsOfEmployeesByLeader(User $leader, Request $request);
}
