<?php


namespace App\Services\Interfaces;


use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;

interface AuthServiceInterface
{
    public function login(LoginRequest $request);
    public function logout();
    public function changePassword(ChangePasswordRequest $request);
}
