<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Timesheet extends BaseModel
{
    protected $table = 'timesheets';

    const STATUS_NOT_APPROVE = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 2;

    protected $fillable = [
        'date',
        'trouble',
        'plan',
        'created_at',
        'updated_at',
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to include only late timesheets.
     *
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeLate(Builder $query)
    {
        $timeDoneTimesheet = Setting::where('key', 'done-timesheet')->first()->value ?? Setting::DEFAULT_TIME_DONE_TIMESHEET;

        return $query->whereColumn(DB::raw('CAST(created_at as date)'), '>', 'date')
            ->orWhere(function ($query) use ($timeDoneTimesheet) {
                $query->whereColumn(DB::raw('CAST(created_at as date)'), '=', 'date')
                    ->whereTime('created_at', '>=', $timeDoneTimesheet);
            });
    }

    public static function statuses(int $key = null) {
        $statuses = [
            self::STATUS_NOT_APPROVE => __('statuses.not-approve'),
            self::STATUS_APPROVED => __('statuses.approved'),
            self::STATUS_REJECTED => __('statuses.rejected'),
        ];

        return !empty($statuses[$key]) ? $statuses[$key] : $statuses;
    }
}
