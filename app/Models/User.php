<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens, Notifiable;

    const AVATAR_DEFAULT = 'https://ramcotubular.com/wp-content/uploads/default-avatar.jpg';

    const ROLE_ADMIN = 'Admin';
    const ROLE_EMPLOYEE = 'Employee';
    const ROLE_LEADER = 'Leader';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $attributes = [
        'avatar' => self::AVATAR_DEFAULT,
    ];

    protected $with = [
        'roles',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function timesheets()
    {
        return $this->hasMany(Timesheet::class);
    }

    public function employees()
    {
        return $this->hasMany(User::class, 'leader_id', 'id');
    }

    public function leader()
    {
        return $this->belongsTo(User::class, 'leader_id');
    }

    /**
     * Determine if user has a role.
     *
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role = '')
    {
        if (empty($role)) {
            return false;
        }

        return $this->roles->contains('name', $role);
    }
}
