<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    const DEFAULT_PER_PAGE = 10;
    const MAX_PER_PAGE = 500;

    public $timestamps = true;
}
