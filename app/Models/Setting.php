<?php

namespace App\Models;


class Setting extends BaseModel
{
    protected $table = 'settings';

    protected $fillable = [
        'key',
        'display_name',
        'value',
    ];

    const DEFAULT_TIME_DO_TIMESHEET = '17:00:00';
    const DEFAULT_TIME_DONE_TIMESHEET = '19:00:00';
}
