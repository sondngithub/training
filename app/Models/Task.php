<?php

namespace App\Models;

class Task extends BaseModel
{
    protected $table = 'tasks';

    protected $fillable = [
        'content',
        'actual_hours',
    ];
}
