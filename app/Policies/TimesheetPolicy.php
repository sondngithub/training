<?php

namespace App\Policies;

use App\Models\User;

class TimesheetPolicy
{
    public function viewDashboard(User $user)
    {
        return $user->hasRole(User::ROLE_EMPLOYEE);
    }

    public function viewEmployeeTimesheets(User $user)
    {
        return $user->hasRole(User::ROLE_LEADER);
    }
}
