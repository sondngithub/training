<?php

namespace App\Console;

use App\Jobs\SendMailForNotifyTime;
use App\Mail\NotifyItIsTimeCompleteTimesheet;
use App\Mail\NotifyItIsTimeToDoTimesheet;
use App\Models\Setting;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->scheduleJobSendMailForNotifyItIsTimeToDoTimesheet($schedule);

        $this->scheduleJobSendMailForNotifyItIsTimeToCompleteTimesheet($schedule);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    protected function scheduleJobSendMailForNotifyItIsTimeToDoTimesheet(Schedule $schedule)
    {
        $timeDoTimesheet = Setting::DEFAULT_TIME_DO_TIMESHEET;

        if ($setting = Setting::where('key', 'do-timesheet')->first()) {
            $timeDoTimesheet = $setting->value;
        }

        $mail = new NotifyItIsTimeToDoTimesheet();

        $this->scheduleSendingMailOfWeekdaysDailyAt($timeDoTimesheet, $schedule, $mail);
    }

    protected function scheduleJobSendMailForNotifyItIsTimeToCompleteTimesheet(Schedule $schedule)
    {
        $timeCompleteTimesheet = Setting::DEFAULT_TIME_DONE_TIMESHEET;

        if ($setting = Setting::where('key', 'done-timesheet')->first()) {
            $timeCompleteTimesheet = $setting->value;
        }

        $mail = new NotifyItIsTimeCompleteTimesheet();

        $this->scheduleSendingMailOfWeekdaysDailyAt($timeCompleteTimesheet, $schedule, $mail);
    }

    protected function scheduleSendingMailOfWeekdaysDailyAt(String $time, Schedule $schedule, Mailable $mail)
    {
        $job = new SendMailForNotifyTime($mail);

        $schedule->job($job)
            ->weekdays()
            ->at($time);
    }
}
