<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'id' => 1,
                'key' => 'do-timesheet',
                'display_name' => __('settings.do-timesheet'),
                'value' => '17:00:00',
            ],
            [
                'id' => 2,
                'key' => 'done-timesheet',
                'display_name' => __('settings.done-timesheet'),
                'value' => '19:00:00',
            ],
        ]);
    }
}
